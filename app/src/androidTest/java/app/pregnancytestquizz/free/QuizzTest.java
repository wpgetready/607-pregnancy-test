package app.pregnancytestquizz.free;

import android.content.Context;
import androidx.test.InstrumentationRegistry;
import app.pregnancytestquizz.free.PieChartQuizz;
import app.pregnancytestquizz.free.Question;
import app.pregnancytestquizz.free.Quizz;

import android.content.Intent;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by i7 on 2019/03/04.
 * Check
 * https://developer.android.com/studio/test
 * To understand what's going on
 */
public class QuizzTest {

    String qResults;

    Context appContext;
    @Before
    public void setUp() throws Exception {
appContext  =InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void testResults1() {
        Intent intent = new Intent(appContext, PieChartQuizz.class);
        intent.putExtra("perCent",26f);
        intent.putExtra("isBoy",false);
        intent.putExtra("answers","empty for the moment");
        //TODO: Faltan pasar los datos al intent.
        appContext.startActivity(intent);
    }

    @Test
    public void countingCategory60() {
        Quizz q = new Quizz(appContext);
        int amount=q.getCategoryAmount(60);
        assertEquals(2,amount);
    }

    //Question missing, even the rest of the data is there
    @Test
    public void questionIntegrityCheckC60Q01 () throws IllegalAccessException {
        int category = 60;
        int question=1;
        Quizz quizz =new Quizz(appContext);
        String q = quizz.getQuestion(category,question);
        assertFalse("The question is empty", q.equals("?"));
        qResults = quizz.getAnswerResults(category, question);
        assertFalse("The Results variable was not found!",qResults.equals("?"));
        int   aQuantity = quizz.getAnswerQuantity(category, question);
        assertFalse("The quantity of Answers does not match with the Results expected", qResults.length()!=aQuantity);
    }


    @Test
    public void questionIntegrityCheckC60Q01_version2 () throws IllegalAccessException {
        int category = 60;
        int question=1;
        Quizz quizz =new Quizz(appContext);

        String q = quizz.getQuestion(category,question);
        assertFalse("The question is empty", q.equals("?"));
        qResults = quizz.getAnswerResults(category, question);
        assertFalse("The Results variable was not found!",qResults.equals("?"));
        int   aQuantity = quizz.getAnswerQuantity(category, question);
        assertFalse("The quantity of Answers does not match with the Results expected", qResults.length()!=aQuantity);
    }


    //If questions is missing, return "?"
    @Test
    public void questionIntegrityCheckC61Q01_QuestionEmpty () throws IllegalAccessException {
        Quizz quizz =new Quizz(appContext);
        Quizz.validQuestion vq = quizz.isQuestionValid(61,1);
        assertEquals(Quizz.validQuestion.QUESTION_TEXT_EMPTY,vq);
    }

    //if there are no Results, return "?"
    @Test
    public void questionIntegrityCheckC62Q01_ResultsMissing () throws IllegalAccessException {
        int category = 62;
        int question=1;
        Quizz quizz =new Quizz(appContext);
        String q = quizz.getQuestion(category,question);
        assertFalse("The question is empty", q.equals("?"));
        qResults = quizz.getAnswerResults(category, question);
        assertTrue("The Results variable was not found!",qResults.equals("?"));
    }

    //if question quantity does not match with results, is wrong.

    @Test
    public void questionIntegrityCheckC63Q01 () throws IllegalAccessException {
        int category = 63;
        int question=1;
        Quizz quizz =new Quizz(appContext);
        String q = quizz.getQuestion(category,question);
        assertFalse("The question is empty", q.equals("?"));
        qResults = quizz.getAnswerResults(category, question);
        assertFalse("The Results variable was not found!",qResults.equals("?"));
        int   aQuantity = quizz.getAnswerQuantity(category, question);
        assertTrue("The quantity of Answers does not match with the Results expected", qResults.length()!=aQuantity);
    }

    //This is a COMPLETE test, to check some aspects of every question.
    @Test
    public void checkQuestions_to_19 () throws IllegalAccessException {
        int catAmount=1;
        Quizz quizz =new Quizz(appContext);
        for(int i=1;i<=19 ; i++) {
            catAmount = quizz.getCategoryAmount(i);
            for (int j=1;j<=catAmount;j++){
                Log.i("QUESTION_TEST","Checking category " + i + ",question " + j);
                checkQuestion(i, j, quizz);
            }
        }
    }

    private void checkQuestion(int category, int question, Quizz quizz) throws IllegalAccessException {
        String q = quizz.getQuestion(category,question);
        assertFalse("The question (cat=" + category + ", q=" + question + ") is empty", q.equals("?"));
        qResults = quizz.getAnswerResults(category, question);
        assertFalse("The Results (cat=" + category + ", q=" + question + ")  were not found!",qResults.equals("?"));
        int   aQuantity = quizz.getAnswerQuantity(category, question);
        assertFalse("Answers quantity (cat=" + category + ", q=" + question + ")  mismatch (" + qResults.length() +"!=" + aQuantity + ")", qResults.length()!=aQuantity);
    }


    @Test
    public void buildQuestions_to_19 () throws IllegalAccessException {
        int catAmount=1;
        List<Question> questions = new ArrayList<Question>();
        Quizz quizz =new Quizz(appContext);
        Question q= null;
        for(int i=1;i<=19 ; i++) {
            catAmount = quizz.getCategoryAmount(i);
            for (int j=1;j<=catAmount;j++){
                Log.i("QUESTION_TEST","Checking category " + i + ",question " + j);

                q= quizz.buildQuestion(i,j);
                questions.add(q);
            }
        }
        Log.i("QUESTION_TEST","All questions loaded");
    }

    @Test
    public void buildQuizzTest() {
        Quizz quizz =new Quizz(appContext);
        List<Question> questionList= quizz.buildQuizz();
        Log.i("QUESTION_TEST","Quizz built");
    }
}
