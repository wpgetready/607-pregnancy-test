package app.pregnancytestquizz.free.Intro;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.pregnancytestquizz.free.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Slide07Fragment extends Fragment {


    public Slide07Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_slide07, container, false);
    }

}
