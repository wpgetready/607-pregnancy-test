
package app.pregnancytestquizz.free;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;

import app.pregnancytestquizz.free.R;
import app.pregnancytestquizz.free.Tools.HiddenShot;
import app.pregnancytestquizz.free.Tools.MyValueFormatter;
import app.pregnancytestquizz.free.Tools.TinyDB;
import app.pregnancytestquizz.free.Tools.Tools;

import static android.view.View.GONE;

/*
This PCQ is an attempt of creating proper graphics for the results.
It took a long way making it, since there was so many problems in here.
This library is NOT friendly and it took a great effort to make it work inside the project
which included:
1-Update Android
2-Migrate to AndroidX
3-Solve several BUGS on the project
4-Deciding finally to import the complete project since it is buggy, at least for the moment until
 simplify the project enough to let MPChartLib reference from the compilation
This is an ongoing documentation

20190318: This became way more complex than  I expected, so I'm testing and finding easier ways
of implementing. Even It was easy for me to implement it, I KNOW I will forget things in the future
, so I'm working NOW for ways of simplifying and documenting overall process.
20190324: I'm getting continous out of memory when trying in Samsung S4.
That's due images are big.So I'm workarounding the problem subsampling the images. (See use of Tools here)
 20190601: working in a new app and simplified version. So , it's clearly enough pregnant /non-pregnant values.

 */


public class PieChartQuizz extends DemoBase implements OnSeekBarChangeListener,
        OnChartValueSelectedListener ,DialogFrag.OnDialogDismissListener{

    TinyDB tinyDB;
    private ConstraintLayout ctlMain;
    private ConstraintLayout ctlCalc;
    private ImageView imgBG;
    private PieChart chart;
    private TextView txtComments;
    private TextView txtTitle;
    private TextView txtTip;
    private Button btnShare;
    private Button btnAbout;
    private Button btnRestartQuizz;

    private ImageView piechartBkg;

    private int[] DrawResults = {R.drawable.pt_results_01,R.drawable.pt_results_02,R.drawable.pt_results_03,R.drawable.pt_results_04};
    private int[] Backgrounds = {R.drawable.bkg01,R.drawable.bkg02,R.drawable.bkg03,R.drawable.bkg04};
    private int[] CircleColor = {Color.argb(128,137,103,181),
                                 Color.argb(192,20,100,255),
                                 Color.argb(128,137,103,181),
                                 Color.argb(128,137,103,181)};

    private int[] ColorCircleBar = {Color.rgb(80,56,141),Color.rgb(255,175,20),
                                    Color.rgb(236,79,20),Color.rgb(235,50,132)};
    /*
    private int[] DrawResults = {R.drawable.boy01,R.drawable.boy02,R.drawable.boy03,R.drawable.boy04,R.drawable.boy05,R.drawable.boy06,
                                R.drawable.girl01,R.drawable.girl02,R.drawable.girl03,R.drawable.girl04,R.drawable.girl05,R.drawable.girl06};
*/
    private int colorBoy = ColorTemplate.VORDIPLOM_COLORS[3];
    private int colorGirl = ColorTemplate.VORDIPLOM_COLORS[4];
    //private int nonConclusive = Color.rgb(202,207,210); //#CACFD2
    private int nonConclusive = Color.argb(128,202,207,210); //#CACFD2
    //Reference colors: https://htmlcolorcodes.com/color-chart/

    private Typeface tf;

    int mProgressStatus =0;
    private Handler mHandler = new Handler();
    private InterstitialAd interstitialAd;
    Boolean adDisplayed=false;
    Boolean setupOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_piechart_quizz);
        tinyDB = new TinyDB(this);

        ctlCalc = findViewById(R.id.calcCL);
        ctlCalc.setVisibility(View.VISIBLE);
        //Hide main view, show calculations
        ctlMain = findViewById(R.id.mainCL);
        ctlMain.setVisibility(View.GONE);
        piechartBkg = findViewById(R.id.piechartBkg);
        setupOnce = false; //avoids any event trying to call setup twice.

        setupButtons();
        setupAds();
        runCalculations();
    }

    private void setupButtons() {
        btnRestartQuizz = findViewById(R.id.btnRestartQuizz);
        btnRestartQuizz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed(); //same functionality than going back from this Layout
            }
        });

        btnShare = findViewById(R.id.btnShare);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnShare.setVisibility(View.GONE);
                verifyStoragePermissions(PieChartQuizz.this);
                HiddenShot.getInstance().buildShotAndShare( PieChartQuizz.this,"This is my Pregnancy Test Quizz result!");
                btnShare.setVisibility(View.VISIBLE);
            }
        });

        btnAbout = findViewById(R.id.btnAbout);
        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFragmentDialog(getString(R.string.ptq_version),getString(R.string.ptq_suggestions));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imgBG.setImageBitmap(null);
        piechartBkg.setImageBitmap(null);
        interstitialAd.setAdListener(null); // https://stackoverflow.com/questions/24902845/memory-leak-while-using-admob-interstitial-ads
    }


    /*
    Initializes ads.
    Reference: BGPT
    20190322: alternate two types of interstitials, video and static ones.
    On this way I'm stopping the Google common behavior of repeating the same interstitials if
    we 'loop' the process.
     */
    private void setupAds() {

        //watch out context see: https://stackoverflow.com/questions/24902845/memory-leak-while-using-admob-interstitial-ads
        interstitialAd = new InterstitialAd(getApplicationContext());
        if (tinyDB.getBoolean("interstitial")){
            interstitialAd.setAdUnitId(getString(R.string.interstitial_text));
            tinyDB.putBoolean("interstitial",false);
        } else {
            interstitialAd.setAdUnitId(getString(R.string.interstitial_video));
            tinyDB.putBoolean("interstitial",true);
        }
        //Logic never fails...until it fails.
        //If we the interstitial can't load for any reason, the ONLY true event should be onAdFailedToLoad.
        //if that happen, the moment of display the ad has been passed. So , I can't show it in the future.
        //Conversely, if we could display it, the activity will fire setup AFTER the ad is closed.
        //So , it is not posible that onAdFailedToLoad and onAdClosed fire at the same time, in the same scenario.
        //Or I least I believe so.
        //In BOTH cases, setupResults should be fired, when display or when not.
        //Also I use a variable to avoid any scenario trying to call setupResults twice.

        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.i("QUIZZ" ,"Ad loaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.e("QUIZZ" ,"Ad failed to load with code" + errorCode);
                setupResults();
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                Log.i("QUIZZ" ,"Ad closed");
                setupResults();
            }
        });

        AdRequest interstitialRequest = new AdRequest.Builder().build();
        interstitialAd.loadAd(interstitialRequest);
    }

    /*
    Simulate a calculation to proper display the interstitials.
    The concept is as follows:
    Wait for a while, display the form, wait a moment and display interstitial.
    20190319:After seeing for a while, this is not THAT simple and it is not that LEGAL also.
    I'm reworking a simpler way. If the add is loaded , wait until the add is loaded.
    Otherwise, well, display the results.
    20190324: I found a bug , where the ad is not loaded, then the app stays calculating and never display results
    Also, the time for loading the ad was extended to make sure the ad is loaded (100 sometimes is not enough)
    Even though, this NEEDS a serious rework.
    Check the example on
    https://developers.google.com/admob/android/interstitial
    with a proper use of timers
    For the moment, I decided if the ad fail, avoid any interstitial event.

    20190611: After reading     https://www.sitepoint.com/java-thread-class-tutorial/
    I've changed the thread. Now I know a thread is finished when it reaches end of execution, so make a return was ok, however it was unnecesary.


     */
    private void runCalculations(){
        ProgressBar progressBar = findViewById(R.id.progressBar);
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                int r = (int) (Math.random() * 50) + 1;
                Log.i("PTQ","random:" + r);
                //Create a delay to display cycles.
                while (mProgressStatus <= 100 + r ) {
                    mProgressStatus += 1;
                    try {
                        Thread.sleep(50);
                        Log.e("PTQ", "Status:" + mProgressStatus);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                //Check if the interstitial was loaded.
                mHandler.post(new Runnable() {
                    public void run() {
                        //if (mProgressStatus == 150) {
                        if (interstitialAd.isLoaded() && !adDisplayed) {
                            Log.i("PTQ", "Displaying interstitial...");
                            interstitialAd.show();
                            adDisplayed = true; //display just once.
                        } else {
                            Log.e("PTQ", "Interstitial failed");
                        }
                        //}
                    }
                });
            }
        });
        thread.start();
        Log.i("PTQ","Thread started");

        /*
        try {
            thread.join(); //wait until finishes.
        } catch (InterruptedException e) {
            Log.i("PTQ","Thread finished");
        }
        Log.i("PTQ","Thread finished");
        */
    }

    private void setupResults() {
        if (setupOnce){
            Log.e("QUIZZ", "Somebody tried to call setupResults TWICE...");
            return;
        }


        setupOnce=true;
        ctlCalc.setVisibility(GONE);
        ctlMain.setVisibility(View.VISIBLE);

        tinyDB = new TinyDB(this);
        //get the fonts from the assets (watchout assets and resources are different folders)
        tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        imgBG = findViewById(R.id.imgBoyGirl);
        txtComments = findViewById(R.id.txtComments);
        txtComments.setMovementMethod(new ScrollingMovementMethod()); //Add scrollbars to the comment

        txtTitle = findViewById(R.id.txtTitle);
        txtTip = findViewById(R.id.txtTip);
        //Solved with font selection ...
        //txtComments.setTypeface(tf);
        //txtTitle.setTypeface(tf);

        setTitle(R.string.pregnancy_quizz);

        //Get data from form which called
        Intent intent = getIntent();
        float perCent = intent.getFloatExtra("perCent",-1.0f);
        //Boolean isBoy = intent.getBooleanExtra("isBoy",true);
        Boolean isBoy = false; //one only style. This is used since we need just pregnant /non pregnant values.
        String answers = (String) intent.getStringExtra("answers");

        txtComments.setText("QUESTION ENDED. Results:(%" + perCent + ")" + " answers: " + answers);
        int arrayValue = setArrayValue(perCent);

        setupBackground(arrayValue);
        setupCenterImage(arrayValue);
        setupChart(perCent, isBoy,arrayValue);

        setupComments(perCent,isBoy);
    }

    private void setupBackground(int arrValue) {
        int myResource = 0;
        myResource = Backgrounds[arrValue];
        //piechartBkg.setImageBitmap(Tools.decodeSampledBitmapFromResource(getResources(),myResource,240,180));
        Tools.setBitmap(this,piechartBkg,myResource);
    }

    private void setupCenterImage(int arrValue) {
        imgBG.setAlpha(0.0f);
        //imgBG.setScaleType(ImageView.ScaleType.);
        imgBG.setScaleX(0.1f);
        imgBG.setScaleY(0.1f);

        /*
        int myBaby = (int)(Math.random()*6); //Create a number between 0..5
        if(!isBoy) {
            myBaby +=6; //if it is a girl, the random value is adding 6 to select proper image in the array
        }
        if (myBaby<0 || myBaby >11){
            Log.e("Quizz","invalid array value " + myBaby);
        }
        */
        imgBG.setImageDrawable(getDrawable(DrawResults[arrValue]));
        imgBG.animate().alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setDuration(3000);

    }

    /**
     * Converts percent into a value betwee [0..3]
     * @param perCent
     * @return
     */
    private int setArrayValue(float perCent){
        int drawValue = (int) (perCent /25.0f); //gives a int value between [0..3]
        if (drawValue<0 || drawValue>3) {
            drawValue=0;
            Log.e("Quizz","invalid array value " + drawValue);
        }
        return drawValue;
    }

    private void fadeBtn(final Button btn) {
        btn.setAlpha(0.0f);
        btn.setVisibility(View.GONE);

        ViewPropertyAnimator vpa = btn.animate().alpha(0.0f).setDuration(5000);
        vpa.setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {
                btn.setVisibility(View.VISIBLE);
                btn.animate().alpha(1.0f).setDuration(1000);
            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
    }

    private void fadeTip() {
        txtTip.setAlpha(0.0f);
        //Wait until wheel stops
        ObjectAnimator a1 = ObjectAnimator.ofFloat(txtTip, "alpha", 0f);
        a1.setDuration(2000);

        //Show tip
        ObjectAnimator a2 = ObjectAnimator.ofFloat(txtTip, "alpha", 1f);
        a2.setDuration(500);

        //Do nothing
        ObjectAnimator a3 = ObjectAnimator.ofFloat(txtTip, "alpha", 1f);
        a3.setDuration(4000);

        //fade out
        ObjectAnimator a4 = ObjectAnimator.ofFloat(txtTip, "alpha", 0f);
        a4.setDuration(500);

        AnimatorSet as = new AnimatorSet();
        as.playSequentially(a1,a2,a3,a4);
        as.start();
    }

    //Split comments between 4 possible scenarios: low(0-25), potential(25-50), strong(50-75), and definitive(75+)
    //Tip: the text was corrected using plugin Grammarly.
    //20190324: Added SuppressLint. Why? this is because R.string.titlex and R.string.partx uses parameters
    //Android sees that this string uses parameters and indicates that string should be use formatted="true" parameter or format the string using other way
    //To avoid this (since this is working ok) I supress the error with this option.
    @SuppressLint("StringFormatInvalid")
    private void setupComments(float perCent, Boolean isBoy) {
        fadeBtn(btnShare);
        fadeBtn(btnRestartQuizz);
        fadeBtn(btnAbout);
        fadeTip();

    String comments;
    String title;
  //  String who = isBoy?getString(R.string.boy):getString(R.string.girl);

    if (perCent<=25f) {
        comments = getString(R.string.part1);
        title= getString(R.string.title1);
    } else if (perCent<=50f) {
        comments = getString(R.string.part2);
        title= getString(R.string.title2);
    } else if (perCent<=75f) {
        comments = getString(R.string.part3);
        title= getString(R.string.title3);
    } else {
        comments = getString(R.string.part4);
        title= getString(R.string.title4);
    }
    comments +=getString(R.string.check_doctor);

        txtComments.setText(comments);
        txtTitle.setText(title);
    }

    private void setupChart(float value, boolean isBoy,int arrValue) {
        chart = findViewById(R.id.chart1);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);

        //Allows the pie chart rotation
        //How te give an starting aceleration?
        chart.setDragDecelerationFrictionCoef(0.95f);

        //Set the font type for the center of the pie chart
        // chart.setCenterTextTypeface(Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf"));
        //Set the text, and also use a spannable text (see documentation in the method)
        //art.setCenterText(generateCenterSpannableText());

        //These are the distance fronm the screen borders.
        chart.setExtraOffsets(15.f, 0.f, 15.f, 0.f);

        //To make a hole in the center there is a extra bunch of commands
        //First enable
        //Second hole color

        chart.setDrawHoleEnabled(true);
        //chart.setHoleColor(Color.WHITE);
        //        //FF8C9D /for girl  , 8CEAFF for boy
        chart.setHoleColor(CircleColor[arrValue]);
        /*
        if (isBoy) {
            chart.setHoleColor(Color.argb(128,140,234,255));
        } else {
            chart.setHoleColor(Color.argb(128,255,140,157));
        }
        */

        chart.setTransparentCircleColor(Color.WHITE);
        //chart.setTransparentCircleAlpha(110);
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

       // chart.setDrawCenterText(true);
        //Angle where the graphic starts. I rotated 45 degrees because the data in % was off the screen.
        chart.setRotationAngle(45);
        // enable rotation of the chart by touch
        //Q: is there a way to get the chart moving?
        //R:Yes, spin method makes it. I don't know how to make an endless loop though
        //But that would be using a timer and a rotation
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
        chart.setOnChartValueSelectedListener(this);
        chart.setDrawRoundedSlices(true); //FZSM: Improve the look a little more...

        /*
        seekBarX.setProgress(2);
        seekBarY.setProgress(100);
*/
        setData2(value, isBoy,arrValue);
        chart.animateY(5000, Easing.EaseInOutQuad);

        //   chart.spin(2000, 0.0f, 360.0f,Easing.EaseOutCubic);
        chart.spin (1000,chart.getRotationAngle(),chart.getRotationAngle() * 180.0f, Easing.EaseOutCubic);
        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setEnabled(false);
    }

    //Version for the Quizz. We only need boyValue (or 100-girlValue)
    private void setData2(float Value,Boolean isBoy, int arrValue){
        ArrayList<PieEntry> entries = new ArrayList<>();
        if (isBoy){
            entries.add(new PieEntry(Value, getString(R.string.boy_results)));
        } else {
          //  entries.add(new PieEntry(Value, getString(R.string.girl_results)));
            entries.add(new PieEntry(Value, getString(R.string.chance_of_being_pregnant)));
        }
        PieEntry pieEntry = new PieEntry(30.0f,"test");
        //entries.add(new PieEntry(100f-Value, getString(R.string.non_conclusive_entry)));
        entries.add(new PieEntry(100f-Value, getString(R.string.non_pregnant)));
        PieDataSet dataSet = new PieDataSet(entries, getString(R.string.quizz_result));
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
      //  dataSet.setValueTextColor(ColorTemplate.COLORFUL_COLORS[0]);

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(ColorCircleBar[arrValue]);
        /*
        if (isBoy){
            colors.add(colorBoy);
        } else
        {
            colors.add(colorGirl);
        }
        */
        colors.add(nonConclusive);
        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        //dataSet.setUsingSliceColorAsValueLineColor(true);

        //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.INSIDE_SLICE);

        //FZSM: Creating a custom format to add % to the results.
        ValueFormatter custom = new MyValueFormatter("%");

        //Set values indicator outside the pie (ej 25.0%) in black
        PieData data = new PieData(dataSet);
        data.setValueFormatter(custom);
        data.setValueTextSize(30f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(tf);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);
        chart.invalidate();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) { }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

        if (e == null)
            return;
        Log.i("VAL SELECTED",
                "Value: " + e.getY() + ", xIndex: " + e.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
    }

    @Override
    public void onNothingSelected() {}

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) { }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}

    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "PiePolylineChartActivity");
    }

    //Contextual help
    @Override
    public void onBackPressed() {
        if (!tinyDB.getBoolean("Review")) {
            AskRatingDialog dialogFragment = new AskRatingDialog();
            dialogFragment.show(getFragmentManager(),
                    getResources().getString(R.string.app_name));
        } else {
            Intent intentone = new Intent(this, MainActivity.class);
            startActivity(intentone);
            finish();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    private void showFragmentDialog(String answer,String comment) {
        FragmentManager fm = getSupportFragmentManager();
        DialogFrag dialogFrag = DialogFrag.newInstance(answer,comment);
        dialogFrag.show(fm, "dialog_frag");
    }

    @Override
    public void onDialogDismissListener(int position) {
        //Nothing to do here.
    }
}

/*
Various documentation and notes


    //Documentation:
    // https://medium.com/androiddevelopers/spantastic-text-styling-with-spans-17b0c16b4568
    //A close look of how this was made, it shows that is SUCKS.
    //Unbelievable that a this times, we should do things like this in this way...
    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda");
        s.setSpan(new RelativeSizeSpan(1.5f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.65f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
        return s;
    }


    20190318: how to make a menu:  Please check project MPChartLib (the complete project I downloaded) and check the examples
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.pie, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    ....

 */