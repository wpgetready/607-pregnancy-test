package app.pregnancytestquizz.free;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import app.pregnancytestquizz.free.R;
import app.pregnancytestquizz.free.Tools.Tools;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import app.pregnancytestquizz.free.Tools.TinyDB;

import java.util.List;

import static android.view.View.GONE;

//import boyorgirlpregnancyquizz.thekingmobile.com.boyorgirlpregnancyquizz.R;
//20190605: New changes for displaying a FragmentDialog

public class QuizzActivity extends AppCompatActivity implements DialogFrag.OnDialogDismissListener{
    int  DISPLAY_ADS_EVERY = 5;

    TinyDB tinyDB;

    Quizz quizz;
    List<Question> questions;
    int qCounter=0;
    boolean flag=true;

    AdView adView;
    private InterstitialAd interstitialAd;
    int adCounter;

    TextView txtQuestion,txtProgress;
    ImageView imageView;
    ImageView imgBack;
    Button btnAnswer1,btnAnswer2,btnAnswer3,btnAnswer4,btnAnswer5,btnAnswer6;
    int result; //acumulated total
    String resultS; //string acumulating all the options taken
    int maxAcum; //max acumulated for calculation percentage
    int minAcum;// min acumulated for calculation percentage

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizz);
        tinyDB = new TinyDB(this);
        //showFragmentDialog(); //Dialog for explaining each answer.

        quizz = new Quizz(this.getBaseContext());
        questions = quizz.buildQuizz();

        txtQuestion = findViewById(R.id.txtQuestion);
        txtProgress = findViewById(R.id.txtProgress);
        imageView = findViewById(R.id.imgQuestion);
        imgBack =findViewById(R.id.imgBack);

        btnAnswer1 = findViewById(R.id.btnAnswer1);
        btnAnswer2 = findViewById(R.id.btnAnswer2);
        btnAnswer3 = findViewById(R.id.btnAnswer3);
        btnAnswer4 = findViewById(R.id.btnAnswer4);
        btnAnswer5 = findViewById(R.id.btnAnswer5);
        btnAnswer6 = findViewById(R.id.btnAnswer6);

        result=0;
        resultS="";

        adCounter=0;

        initAds();

        //resetActivity(); //reset the activity with default values.
        loadQuestion();



    }

    private void setBackButton() {

        if (qCounter==0){
            imgBack.setVisibility(GONE);
            return;
        }
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qCounter--;
                loadQuestion(); //Let's go back a step...
            }
        });
    }

    private void showFragmentDialog(String answer,String comment) {
        FragmentManager fm = getSupportFragmentManager();
        DialogFrag dialogFrag = DialogFrag.newInstance("Your answer:\n" + answer,comment);
        dialogFrag.show(fm, "dialog_frag");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imageView.setImageBitmap(null);
        interstitialAd.setAdListener(null); // https://stackoverflow.com/questions/24902845/memory-leak-while-using-admob-interstitial-ads
    }


    //Load the question into the form
    private void loadQuestion() {
        Question q=questions.get(qCounter);
        int currentQ = qCounter+1;
        txtProgress.setText((currentQ + "/19"));
        txtQuestion.setText(q.getText());

        //imageView.setImageResource(q.getImgResource());

        //imageView.setImageBitmap(Tools.decodeSampledBitmapFromResource(getResources(),q.getImgResource(),240,180));
        //EXACTLY the same as above commented but shorter
        imageView.setImageBitmap(null); //empty image, to avoid leaking.
        Tools.setBitmap(this,imageView,q.getImgResource());

        //Set buttons according how many answer we have.
        //In fact, we always have at least two options so we have to check
        //only option 3 or 4.
        btnAnswer1.setText(q.getAnswers().get(0).getText());
        btnAnswer2.setText(q.getAnswers().get(1).getText());
        btnAnswer3.setVisibility(GONE);
        btnAnswer4.setVisibility(GONE);
        btnAnswer5.setVisibility(GONE);
        btnAnswer6.setVisibility(GONE);

        if (q.getAnswers().size()>2) {
            btnAnswer3.setVisibility(View.VISIBLE);
            btnAnswer3.setText(q.getAnswers().get(2).getText());
        }

        if (q.getAnswers().size()>3) {
            btnAnswer4.setVisibility(View.VISIBLE);
            btnAnswer4.setText(q.getAnswers().get(3).getText());
        }

        if (q.getAnswers().size()>4) {
            btnAnswer5.setVisibility(View.VISIBLE);
            btnAnswer5.setText(q.getAnswers().get(4).getText());
        }

        if (q.getAnswers().size()>5) {
            btnAnswer6.setVisibility(View.VISIBLE);
            btnAnswer6.setText(q.getAnswers().get(5).getText());
        }

        animateQuizz(q);
        setBackButton();
    }

    private void animateQuizz(Question q){

        float offset = 1000.0f;
        int timeSpan = 2000;

        imageView.setAlpha(0.0f);
        imageView.setY(-800.0f);
        flag =!flag;
        int sign = flag?1:-1;
        imageView.setRotation(90.0f * sign);
        imageView.animate().translationY(0.0f).alpha(1.0f).rotation(0.0f).setDuration(timeSpan);

        txtQuestion.setAlpha(0.0f);
        txtQuestion.animate().alpha(1.0f).setDuration(timeSpan);

        btnEffect(btnAnswer1, offset,timeSpan);
        btnEffect(btnAnswer2,-offset,timeSpan);

        if (q.getAnswers().size()>2) {
            btnEffect(btnAnswer3,offset,timeSpan);
        }

        if (q.getAnswers().size()>3) {
            btnEffect(btnAnswer4,-offset,timeSpan);
        }

        if (q.getAnswers().size()>4) {
            btnEffect(btnAnswer5,offset,timeSpan);
        }

        if (q.getAnswers().size()>5) {
            btnEffect(btnAnswer6,-offset,timeSpan);
        }

        imgBack.setAlpha(0.0f);
        imgBack.setRotation(180.0f * sign);
        imgBack.animate().alpha(1.0f).rotation(0.0f).setDuration(timeSpan);
    }

    private void btnEffect(Button btn, float offset, int timeSpan) {
        btn.setAlpha(0.0f);
        btn.setX(-offset);
        btn.animate().translationX(0.0f).alpha(1.0f).setDuration(timeSpan);
    }

    //What happen when a button is clicked
    public void btnClick(View view) {
        Question q=questions.get(qCounter);
        Button clickedBtn = (Button)view;
        int tag = Integer.parseInt(clickedBtn.getTag().toString()); //get a value between 1..6
        Answer selectedAnswer = q.answers.get(tag);
        int pv = selectedAnswer.getPonderateValue();
        result +=pv;
        maxAcum +=q.getMaxAnswer();
        minAcum +=q.getMinAnswer();
        resultS +=selectedAnswer.getResultValue();

        //20190605: Display the advice for this question, based on the answer provided.
        showFragmentDialog(selectedAnswer.getText(),selectedAnswer.getTextExplain());
        //20190606: Befor the fragment dialog, all the code on onDialogDismissListener was here.
        //The issue was the dialog was displayed but the app continued working, which is was weird.
        //To sort it out, we implemented an interface who is waiting until button ok is pressed.

    }

    @Override
    public void onDialogDismissListener(int position) {
        //We've done here. Next step is wait a bit, making an effect and pass to the other question until we have done.
     //   Log.i("POINTS", "Ponderate Value=" + pv + ",result = " + result + ",max=" + maxAcum + ",min=" + minAcum + ",resultS=" + resultS );
        displayInterstitial(); //dislay interstitial according adCounter variable

        if (qCounter < questions.size()-1){
            qCounter++;
            loadQuestion();
        } else
        {
            float perCent =0.0f;
            //Calculate percentage based on results.
            if (result>0) {
                perCent = (float)result / maxAcum * 100;
            } else {
                perCent = (float) result / minAcum * 100;
            }

            Intent intent = new Intent(this,PieChartQuizz.class);
            intent.putExtra("perCent",perCent);
            intent.putExtra("isBoy",result>0);
            intent.putExtra("answers",resultS);
            //TODO: Faltan pasar los datos al intent.
            startActivity(intent);

            //Toast.makeText(this, "QUESTION ENDED. Results:" + result + "maxAcum = " + maxAcum + ",minAcum= " + minAcum + ", " + itsa + "(%" + perCent + ")" + " answers: " + resultS, Toast.LENGTH_LONG).show();
            //Make some flag to indicate the quizz is over, disabling the options or whatever.

        }
    }

    private void displayInterstitial() {
        adCounter++;
        if  (adCounter % DISPLAY_ADS_EVERY == 0) {
            if (interstitialAd.isLoaded()) {
                interstitialAd.show();
            } else
            {
                Log.i("PTQ","Error: interstitial not loaded: counter= " + adCounter);
            }
        } else
        {
            Log.i("PTQ", "adCounter = " + adCounter);
        }
    }

    private void initAds() {
        //Setup banner ads
        adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        //Setup interstitial ads
        // watch out context: see https://stackoverflow.com/questions/24902845/memory-leak-while-using-admob-interstitial-ads
        interstitialAd = new InterstitialAd(getApplicationContext());
        setupInterstitialType();

        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.i("PTQ" ,"Ad loaded");
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.e("PTQ" ,"Ad failed to load with code" + errorCode);
            }

            @Override
            public void onAdOpened() {}

            @Override
            public void onAdLeftApplication() {}

            @Override
            public void onAdClosed() {
                Log.i("PTQ" ,"Ad closed");

                interstitialAd.loadAd(new AdRequest.Builder().build());
                setupInterstitialType();   //Make sure reload a new interstitial every time
            }
        });

        interstitialAd.loadAd(new AdRequest.Builder().build());
    }

    //Forces interstitial type rotation
    private void setupInterstitialType() {
        if (tinyDB.getBoolean("interstitial")){
            interstitialAd.setAdUnitId(getString(R.string.interstitial_text));
            Log.i("PTQ","Interstitial text set");
            tinyDB.putBoolean("interstitial",false);
        } else {
            interstitialAd.setAdUnitId(getString(R.string.interstitial_video));
            Log.i("PTQ","Interstitial video set");
            tinyDB.putBoolean("interstitial",true);
        }
    }
}