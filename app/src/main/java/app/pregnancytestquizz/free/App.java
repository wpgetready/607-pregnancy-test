package app.pregnancytestquizz.free;

import android.app.Application;

import com.google.android.gms.ads.MobileAds;
import com.onesignal.OneSignal;

public class App extends Application {
    public static final String LOG_TAG = App.class.getSimpleName();

    //AdView banner;
    @Override
    public void onCreate() {
        super.onCreate();

        //MobileAds.initialize(this);
        MobileAds.initialize(this, getString(R.string.admob_app_id));

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

      /*
        banner = new AdView(this);
        requestBanner();
        */
    }

    /*
    public void requestBanner(){
        banner.setAdUnitId(getString(R.string.banner));
        banner.setAdSize(AdSize.SMART_BANNER);
        banner.loadAd(new AdRequest.Builder().build());
    }

    public void loadAd(ConstraintLayout layAd) {
        if (banner.getParent() != null) {
            ViewGroup tempVg = (ViewGroup) banner.getParent();
            tempVg.removeView(banner);
        }
        layAd.addView(banner);
    }
    */
}