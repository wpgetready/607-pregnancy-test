package app.pregnancytestquizz.free;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i7 on 2019/03/03.
 */

public class Question {
    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public int getCategory() {
        return Category;
    }

    public void setCategory(int category) {
        Category = category;
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void addAnswer(Answer answer) {
        answers.add(answer);

        maxAnswer =Math.max(maxAnswer,answer.getPonderateValue());
        minAnswer =Math.min(minAnswer,answer.getPonderateValue());
    }
    /*
    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
    */



    public int getImgResource() {
        return imgResource;
    }

    public void setImgResource(int imgResource) {
        this.imgResource = imgResource;
    }

    public int getMaxAnswer() {
        return maxAnswer;
    }

    public void setMaxAnswer(int maxAnswer) {
        this.maxAnswer = maxAnswer;
    }

    public int getMinAnswer() {
        return minAnswer;
    }

    public void setMinAnswer(int minAnswer) {
        this.minAnswer = minAnswer;
    }

    int maxAnswer=0; //return maximal value for the answer for this question. This is used for statistic calculations
    int minAnswer=0; //return minimal value for the answer for this question. This is used for statistics calculations


    int imgResource;

    String Text;
    int Category;
    int Number;
    List<Answer> answers = new ArrayList<Answer>();
}
