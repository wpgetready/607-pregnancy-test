package app.pregnancytestquizz.free;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.Button;
import android.widget.ImageView;

import app.pregnancytestquizz.free.R;
import app.pregnancytestquizz.free.Tools.TinyDB;
import app.pregnancytestquizz.free.Tools.Tools;

import androidx.appcompat.app.AppCompatActivity;
//import boyorgirlpregnancyquizz.thekingmobile.com.boyorgirlpregnancyquizz.R;

public class MainActivity extends AppCompatActivity {

    Button btnStart;
    ImageView bkg;
    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //This lines should be executed JUST once in the app's lifetime
       // MobileAds.initialize(this,"ca-app-pub-3940256099942544~3347511713");

        tinyDB = new TinyDB(this);
        /*Just for checking results, not active*/
        if (false) {
            Intent intent = new Intent(this,PieChartQuizz.class);
            intent.putExtra("perCent",85.0f);
            intent.putExtra("isBoy",false);
            intent.putExtra("answers","empty for the moment");
            startActivity(intent);
            return;
        }

        //Display Intro just once in the app' lifetime
        if (!tinyDB.getBoolean("Intro")){
       // if (false){
            tinyDB.putBoolean("Intro",true);
            Intent intent = new Intent(this,IntroActivity.class);
            startActivity(intent);
        }

        final Context ctx = this;
        btnStart = findViewById(R.id.btnStart);
        btnStart.setAlpha(0.0f);

        bkg = findViewById(R.id.imgLogo);
        //ERROR/BUG: we can't check width/height on view when creation, since it will return 0 (it's absurd but true)
        //So it's incorrect using getWidth, getHeight
        //bkg.setImageBitmap(Tools.decodeSampledBitmapFromResource(getResources(),R.drawable.pregnancyquizlogo1600x1200,bkg.getWidth(),bkg.getWidth()));

        //bkg.setImageBitmap(Tools.decodeSampledBitmapFromResource(getResources(),R.drawable.pregnancyquizlogo1600x1200,240,180));
        //EXACTLY the same as above, but shorter and some parameters already provided. In case of test there is a overloaded method.
        Tools.setBitmap(this,bkg,R.drawable.ptqlogo900x1600);


        ViewPropertyAnimator vpa= bkg.animate().alpha(1.0f).setDuration(2000);
        vpa.setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {}

            @Override
            public void onAnimationEnd(Animator animator) {

                //Incredible: setEnabled(true/false) DECIDES if the button is visible or not(?! WTF?!)
                btnStart.animate().alpha(1.0f).setDuration(2000);
                /*
                ViewPropertyAnimator vpa = btnStart.animate().alpha(1.0f).setDuration(2000);
                vpa.setListener(new Animator.AnimatorListener() {

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        btnStart.setEnabled(true);
                    }

                    @Override
                    public void onAnimationStart(Animator animation) { }
                    @Override
                    public void onAnimationCancel(Animator animation) { }

                    @Override
                    public void onAnimationRepeat(Animator animation) { }
                });
                */
/*This simply doesn't work
                Animation animation = new AlphaAnimation(0.0f,1.0f);
                animation.setDuration(1000);
                btnStart.startAnimation(animation);
                */

            }

            @Override
            public void onAnimationCancel(Animator animator) {}

            @Override
            public void onAnimationRepeat(Animator animator) {}
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx,QuizzActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bkg.setImageBitmap(null); //Prevents bitmap leaking
    }
}