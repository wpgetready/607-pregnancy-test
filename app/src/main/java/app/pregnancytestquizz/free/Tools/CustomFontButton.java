package app.pregnancytestquizz.free.Tools;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import app.pregnancytestquizz.free.R;

//This class extends button class allowing having button with customized fonts.
//https://stackoverflow.com/questions/43449787/set-custom-font-for-navgiation-view-header
//References:
//BoyorGirl PT code
//Proyecto desarrollador
//Adapted to AndroidX, to see compatibility check https://developer.android.com/jetpack/androidx/migrate

public class CustomFontButton extends androidx.appcompat.widget.AppCompatButton {

    public CustomFontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFontButton(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "OpenSans-Regular.ttf");
        setTypeface(font);
        setTextSize(18.0f);
        setTextColor(getResources().getColor(R.color.white));

    }

}
