package app.pregnancytestquizz.free.Tools;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

/**
 * The following class was created for addressing an issue with Android Studio:
 * 'big' images (anything above 1600x1200 (!)) cause a memory problem when loading directly from resources.
 *  https://developer.android.com/topic/performance/graphics/load-bitmap#java
 * The solution consists in loading a 'ScaledDown' version of the image and decoding it.
 * (it means we decode a compressed image into the uncompressed one without dealing inside the activity)
 * For example, an image of 1920x1080 probably takes 12-20MB , but decoded it is exactly 1920x1080 = about 2MB.
 * Even though, it is incredible the problems I have to handle due the poor ways of solving it from AS...
 */
public class Tools {

    //This function calculate what is the sample size to make sure there is no problem of space.
    private  static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true; //avoids memory allocation, returning null for bitmap object but setting otuWidht,outHeight and outMimeType.
        BitmapFactory.decodeResource(res, resId, options); //returns image dimensions on options even the image is big.
        Log.i("QUIZZ", "Decoded resolution w:" + options.outWidth + " h: " + options.outHeight + " mimetype: " + options.outMimeType);
        Log.i("QUIZZ", "Current sampleSize: " + options.inSampleSize);


        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        Log.i("QUIZZ", "New sampleSize: " + options.inSampleSize);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    //This is a safe use of the image so far.
    public static void setBitmap(Context ctx,ImageView imageView,int resource) {
        setBitmap(ctx,imageView,resource,240,180);
    }

    public static void setBitmap (Context ctx, ImageView imageView, int Resource, int width, int height) {
        //ctlMain.setImageBitmap(Tools.decodeSampledBitmapFromResource(getResources(),R.drawable.pregnancyquizlogo1600x1200,80,60));
        imageView.setImageBitmap(decodeSampledBitmapFromResource(ctx.getResources(),Resource,width,height));
    }
}
